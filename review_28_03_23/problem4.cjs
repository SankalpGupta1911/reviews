let data = require('./1-arrays-jobs.cjs');

let result = data.reduce((acc, employee)=>{
    let salary = parseFloat(employee['salary'].replace('$', ''))
    acc+=salary;
    return acc;

},0)

console.log(result);