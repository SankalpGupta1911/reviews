let data = require("./1-arrays-jobs.cjs");

let stats = data.reduce((acc, iter) => {
  acc[iter["location"]] = acc[iter["location"]] || [0, 0];
  acc[iter["location"]][0] += 1;
  acc[iter["location"]][1] += parseFloat(iter["salary"].replace("$", ""));

  return acc;
}, {});

let result = Object.entries(stats).reduce((acc, iter) => {
  acc[iter[0]] = iter[1][1] / iter[1][0];
  return acc;
}, {});

console.log(result);
